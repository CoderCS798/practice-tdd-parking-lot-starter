package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_a_parking_ticket_when_park_the_car_given_a_car(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(4);
        Ticket parkingTicket = parkingLot.park(car);
        Assertions.assertNotNull(parkingTicket);
    }
    @Test
    void should_return_a_parked_car_ticket_when_fetch_the_car_given_a_parking_ticket(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(4);
        Ticket parkingTicket = parkingLot.park(car);
        Car parkedCar = parkingLot.fetch(parkingTicket);
        Assertions.assertEquals(car,parkedCar);
    }
    @Test
    void should_return_two_parked_car_ticket_when_fetch_the_cars_twice_given_two_parking_tickets(){
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot(4);
        Ticket parkingTicket1 = parkingLot.park(car1);
        Ticket parkingTicket2 = parkingLot.park(car2);
        Car parkedCar1 = parkingLot.fetch(parkingTicket1);
        Car parkedCar2 = parkingLot.fetch(parkingTicket2);
        Assertions.assertEquals(car1,parkedCar1);
        Assertions.assertEquals(car2,parkedCar2);
    }
    @Test
    void should_return_error_message_when_fetch_the_car_given_a_wrong_ticket(){
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot(4);
        Ticket parkingTicket1 = parkingLot.park(car1);
        Ticket wrongTickt = new Ticket();
//        Car parkedCar1 = parkingLot.fetch(wrongTickt);
        WrongTicketException wrongTicketException = Assertions.assertThrows(WrongTicketException.class,()->parkingLot.fetch(wrongTickt));
        Assertions.assertEquals("Unrecognized parking ticket.",wrongTicketException.getMessage());

    }
    @Test
    void should_return_nothing_when_fetch_the_cars_given_a_used_ticket(){
        Car car1 = new Car();
        ParkingLot parkingLot = new ParkingLot(4);
        Ticket parkingTicket1 = parkingLot.park(car1);
        Car parkedCar1 = parkingLot.fetch(parkingTicket1);
//        Car parkAgainCar = parkingLot.fetch(parkingTicket1);
        WrongTicketException wrongTicketException = Assertions.assertThrows(WrongTicketException.class,()->parkingLot.fetch(parkingTicket1));
        Assertions.assertEquals("Unrecognized parking ticket.",wrongTicketException.getMessage());

    }
    @Test
    void should_return_nothing_when_park_the_car_given_a_parking_lot_without_positon(){
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car car4 = new Car();
        Car car5 = new Car();
        ParkingLot parkingLot = new ParkingLot(4);
        parkingLot.park(car1);
        parkingLot.park(car2);
        parkingLot.park(car3);
        parkingLot.park(car4);
//        Ticket parkingTicket5 = parkingLot.park(car5);
        WrongTicketException wrongTicketException = Assertions.assertThrows(WrongTicketException.class,()->parkingLot.park(car5));
        Assertions.assertEquals("No available position.",wrongTicketException.getMessage());

    }

}
