package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_park_the_car_given_a_car_and_parking_boy(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLotList = new ArrayList<>();
        StandardParkingBoy standardParkingBoy =  new StandardParkingBoy(parkingLotList);
        parkingLotList.add(parkingLot);
        Ticket parkingTicket = standardParkingBoy.park(car);
        Assertions.assertNotNull(parkingTicket);
    }
    @Test
    void should_return_a_parked_car_ticket_when_fetch_the_car_given_a_parking_ticket_and_parking_boy(){
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLotList = new ArrayList<>();
        StandardParkingBoy standardParkingBoy =  new StandardParkingBoy(parkingLotList);
        parkingLotList.add(parkingLot);
        Ticket parkingTicket = standardParkingBoy.park(car);
        Car parkedCar = standardParkingBoy.fetch(parkingTicket);
        Assertions.assertEquals(car,parkedCar);
    }
    @Test
    void should_return_two_parked_car_ticket_when_fetch_the_cars_twice_given_two_parking_tickets(){
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLotList = new ArrayList<>();
        StandardParkingBoy standardParkingBoy =  new StandardParkingBoy(parkingLotList);
        parkingLotList.add(parkingLot);
        Ticket parkingTicket1 = standardParkingBoy.park(car1);
        Ticket parkingTicket2 = standardParkingBoy.park(car2);
        Car parkedCar1 = standardParkingBoy.fetch(parkingTicket1);
        Car parkedCar2 = standardParkingBoy.fetch(parkingTicket2);
        Assertions.assertEquals(car1,parkedCar1);
        Assertions.assertEquals(car2,parkedCar2);
    }
    @Test
    void should_return_error_message_when_fetch_the_car_given_a_wrong_ticket(){
        Car car1 = new Car();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        ParkingLot parkingLot = new ParkingLot(10);
        StandardParkingBoy standardParkingBoy =  new StandardParkingBoy(parkingLotList);
        parkingLotList.add(parkingLot);
        standardParkingBoy.park(car1);
        Ticket wrongTickt = new Ticket();
        WrongTicketException wrongTicketException = Assertions.assertThrows(WrongTicketException.class,()-> standardParkingBoy.fetch(wrongTickt));
        Assertions.assertEquals("Unrecognized parking ticket.",wrongTicketException.getMessage());
    }
    @Test
    void should_return_nothing_when_fetch_the_cars_given_a_used_ticket(){
        Car car1 = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLotList = new ArrayList<>();
        StandardParkingBoy standardParkingBoy =  new StandardParkingBoy(parkingLotList);
        parkingLotList.add(parkingLot);
        Ticket parkingTicket1 = standardParkingBoy.park(car1);
        standardParkingBoy.fetch(parkingTicket1);
        WrongTicketException wrongTicketException = Assertions.assertThrows(WrongTicketException.class,()-> standardParkingBoy.fetch(parkingTicket1));
        Assertions.assertEquals("Unrecognized parking ticket.",wrongTicketException.getMessage());
    }
    @Test
    void should_return_nothing_when_park_the_car_given_a_parking_lot_without_positon(){
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        Car car4 = new Car();
        Car car5 = new Car();
        ParkingLot parkingLot = new ParkingLot(4);
        List<ParkingLot> parkingLotList = new ArrayList<>();
        StandardParkingBoy standardParkingBoy =  new StandardParkingBoy(parkingLotList);
        parkingLotList.add(parkingLot);
        standardParkingBoy.park(car1);
        standardParkingBoy.park(car2);
        standardParkingBoy.park(car3);
        standardParkingBoy.park(car4);
        WrongTicketException wrongTicketException = Assertions.assertThrows(WrongTicketException.class,()-> standardParkingBoy.park(car5));
        Assertions.assertEquals("No available position.",wrongTicketException.getMessage());
    }
    @Test
    void should_return_parking_lot1_ticket_when_park_given_a_car_and_two_parking_lots(){
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        List<ParkingLot> parkingLotList = new ArrayList<>();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLotList);
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        Car car = new Car();
        Ticket parkingTicket = standardParkingBoy.park(car);
        Assertions.assertEquals(parkingLot1.parkingList.get(parkingTicket),car);
        Assertions.assertNotEquals(parkingLot2.parkingList.get(parkingTicket),car);
    }
    @Test
    void should_return_parking_lot2_ticket_when_park_given_a_car_and_two_parking_lots_which_first_is_full(){
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(10);
        List<ParkingLot> parkingLotList = new ArrayList<>();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLotList);
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        Car car = new Car();
        Ticket parkingTicket = standardParkingBoy.park(car);
        Assertions.assertEquals(parkingLot2.parkingList.get(parkingTicket),car);
    }
    @Test
    void should_return_right_car_when_fetch_car_twice_given_two_tickets_and_two_parking_lots_with_a_parked_car(){
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        List<ParkingLot> parkingLotList = new ArrayList<>();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLotList);
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket parkingTicket1 = standardParkingBoy.park(car1);
        Ticket parkingTicket2 = standardParkingBoy.park(car2);
        Car parkedCar1 = standardParkingBoy.fetch(parkingTicket1);
        Car parkedCar2 = standardParkingBoy.fetch(parkingTicket2);
        Assertions.assertEquals(car1,parkedCar1);
        Assertions.assertEquals(car2,parkedCar2);
    }
    @Test
    void should_return_error_message_when_fetch_car_given_two_parking_lots_and_unrecognized_ticket(){
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        List<ParkingLot> parkingLotList = new ArrayList<>();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLotList);
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        Ticket unrecognizedTicket = standardParkingBoy.park();
        WrongTicketException wrongTicketException = Assertions.assertThrows(WrongTicketException.class,()-> standardParkingBoy.fetch(unrecognizedTicket));
        Assertions.assertEquals("Unrecognized parking ticket.",wrongTicketException.getMessage());
    }
    @Test
    void should_return_error_message_when_fetch_car_given_two_parking_lots_and_used_ticket(){
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        List<ParkingLot> parkingLotList = new ArrayList<>();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLotList);
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        Car parkedCar = new Car();
        Ticket usedTicket = standardParkingBoy.park(parkedCar);
        standardParkingBoy.fetch(usedTicket);
        WrongTicketException wrongTicketException = Assertions.assertThrows(WrongTicketException.class,()-> standardParkingBoy.fetch(usedTicket));
        Assertions.assertEquals("Unrecognized parking ticket.",wrongTicketException.getMessage());
    }
    @Test
    void should_return_error_message_when_park_car_given_two_parking_lots_without_position(){
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        List<ParkingLot> parkingLotList = new ArrayList<>();
        StandardParkingBoy standardParkingBoy = new StandardParkingBoy(parkingLotList);
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        standardParkingBoy.park(car1);
        standardParkingBoy.park(car2);
        WrongTicketException wrongTicketException = Assertions.assertThrows(WrongTicketException.class,()-> standardParkingBoy.park(car3));
        Assertions.assertEquals("No available position.",wrongTicketException.getMessage());
    }
}
