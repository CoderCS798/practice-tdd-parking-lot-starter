package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StandardParkingBoy {
    private final List<ParkingLot> parkingLotsList;
    public StandardParkingBoy(List<ParkingLot> parkingLotsList) {
        this.parkingLotsList = parkingLotsList;
    }
    public ParkingLot findFirstParkinglot(List<ParkingLot> parkingLotsList){
        List<ParkingLot> validParkingLots = parkingLotsList.stream().filter(parkingLot -> !parkingLot.isFull()).collect(Collectors.toList());
        return validParkingLots.size() == 0 ? null : validParkingLots.get(0);
    }

    public Ticket park(Car car) {
        ParkingLot parkingLot = findFirstParkinglot(parkingLotsList);
        if (parkingLot.isFull()) {
            throw new WrongTicketException("No available position.");
        }
        return parkingLot.park(car);
    }

    public Car fetch(Ticket parkingTicket) {
        ParkingLot inParkingLot = null;
        for (ParkingLot parkingLot : parkingLotsList) {
            if (parkingLot.isValidTicket(parkingTicket)) {
                inParkingLot = parkingLot;
            }
        }
        if (inParkingLot == null) {
            throw new WrongTicketException("Unrecognized parking ticket.");
        }
        return inParkingLot.fetch(parkingTicket);
    }

    public Ticket park() {
        return null;
    }
}
