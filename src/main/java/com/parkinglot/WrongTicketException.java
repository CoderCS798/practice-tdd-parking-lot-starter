package com.parkinglot;

public class WrongTicketException extends RuntimeException{
    public WrongTicketException(String message){super(message);}
}
