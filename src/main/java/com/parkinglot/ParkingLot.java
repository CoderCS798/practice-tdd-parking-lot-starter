package com.parkinglot;

import java.util.*;

public class ParkingLot {
    Map<Ticket,Car>parkingList = new HashMap<>();

    private final int parkingCapacity;

    public ParkingLot(int capacity) {
        this.parkingCapacity = capacity;
        }



    public boolean isFull(){
    //Note the equal sign.
        return parkingList.size()>= parkingCapacity;
    }
    public boolean isValidTicket(Ticket parkingTicket){
        return parkingList.get(parkingTicket) != null;
    }
    public Ticket park(Car car) throws WrongTicketException{
        if(isFull()){
            throw new WrongTicketException("No available position.");
        }
        Ticket parkingTicket = new Ticket(this);
        parkingList.put(parkingTicket,car);
        return parkingTicket;
    }

    public Car fetch(Ticket parkingTicket) throws  WrongTicketException{
        Car parkedCar = parkingList.get(parkingTicket);
        parkingList.remove(parkingTicket);
        if (parkedCar == null){
            throw new WrongTicketException("Unrecognized parking ticket.");
        }
        return parkedCar;
    }
}
