## O:

Today's lesson is mainly about writing code，through the CoderReview session in the morning of the past few days made my JAVA level up a lot.

## R：

I feel it is  a little burdensome.

## I：

Through constant Tasking and Coding I have gained a deeper understanding of TDD and OOP, as well as rapidly improving my JAVA programming skills. In the process of learning TDD, I also know that unit testing is very important in actual project development, so that I can quickly locate the error and debug it in time.

## D:

I will stick to TDD for programming in future